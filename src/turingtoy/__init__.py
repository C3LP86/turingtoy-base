from typing import (
    Dict,
    List,
    Optional,
    Tuple,
)

import poetry_version

__version__ = poetry_version.extract(source_file=__file__)


class MachineHistory:
    current_state: str
    current_reading: str
    current_position: int
    current_memory: str
    current_transition: str

    def __init__(self) -> None:
        self.history = []

    def record(self, state, reading, position, memory, transition):
        self.history.append(self.to_dict())
        self.current_state = state
        self.current_reading = reading
        self.current_position = position
        self.current_memory = memory
        self.current_transition = transition

    def to_dict(self):
        return {
            "state": self.current_state,
            "reading": self.current_reading,
            "position": self.current_position,
            "memory": self.current_memory,
            "transition": self.current_transition,
        }


def replace_character(string, char, index):
    """
    Replace a character in a string at a specified index and return the modified string.
    """
    res = list(string)
    res[index] = char
    return "".join(res)


def run_machine(
    machine: Dict, input_str: str, max_steps: Optional[int] = None
) -> Tuple[str, List[Dict[str, any]], bool]:
    final_states = machine["final states"]
    transition_table = machine["table"]
    blank_symbol = machine["blank"]
    history = MachineHistory()
    state = history.current_state = machine["start state"]
    position = history.current_position = 0
    memory = history.current_memory = input_str
    reading = history.current_reading = input_str[0]
    transition = history.current_transition = transition_table[state][reading]
    step_count = 0
    
    while (state not in final_states) and (
        max_steps is None or step_count < max_steps
    ):  
        if isinstance(transition, dict):
            state = transition.get("L", state)
            memory = replace_character(
                memory, transition.get("write", reading), position
            )
            position += 1 if "R" in transition else -1
        else:
            position += 1 if "R" in transition else -1

        if len(memory) <= position:
            memory += blank_symbol
        elif position < 0:
            memory = blank_symbol + memory
            position = 0
        
        reading = memory[position]
        
        try:
            transition = transition_table[state][reading]
        except KeyError:
            pass
        
        history.record(
            state,
            reading,
            position,
            memory,
            transition,
        )
        step_count += 1
    
    return history.current_memory.strip(blank_symbol), history.history, state in final_states


def execute_turing_machine(
    machine: Dict, input_str: str, max_steps: Optional[int] = None
) -> Tuple[str, List[Dict[str, any]], bool]:
    return run_machine(machine, input_str, max_steps)
